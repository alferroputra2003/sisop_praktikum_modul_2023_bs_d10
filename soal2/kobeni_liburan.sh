##!bin/bash

problem1()
  { 
  no_folder=1

  while [ -d kumpulan_$no_folder ] 
  do no_folder=$((no_folder+1))
  done

  mkdir kumpulan_$no_folder

 WAKTU=$(date +"%H")

  cd kumpulan_$no_folder

  if [[ $WAKTU -eq 00:00 && $(date +"%H") ]]
  then
    WAKTU=1
  fi

  # Loop sebanyak var waktu
  for ((no_file=1; no_file<=$WAKTU; no_file++))
  do
    wget -O perjalanan_$no_file.jpg https://loremflickr.com/320/240/indonesia
  done
}

problem2(){
  nomor=1
  while [ -d kumpulan_$nomor ]
  do 
    # apabila sudah dikompres, maka skip
    if [ -f devil_$nomor.zip ]
    then
      nomor=$((nomor+1))
      continue
    fi

    zip -r  devil_$nomor.zip kumpulan_$nomor
    nomor=$((nomor+1))
  done
}

if [ ! $1 ]
then 
  echo "Error: You must enter number 1 or 2"
  exit
fi

if [ $1 -eq 1 ]
then
    problem1
elif [ $1 -eq 2 ]
then
    problem2
fi
