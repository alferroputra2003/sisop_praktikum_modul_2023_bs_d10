#!bash
echo "nomor 1a"
sort -t, -k21rn '2023 QS World University Rankings.csv' | awk -F',' '/JP/{print $2}' | head -5
echo " "
echo "nomor 1b"
sort -t, -k9n '2023 QS World University Rankings.csv' | awk -F',' '/JP/{print $2}' | head -5
echo " "
echo "nomor 1c"
sort -t, -k20,20n '2023 QS World University Rankings.csv' | awk -F',' '/Japan/ {print $2}' | head -10
echo " "
echo "nomor 1d"
awk -F',' '/Keren/ {print $2}' '2023 QS World University Rankings.csv'
