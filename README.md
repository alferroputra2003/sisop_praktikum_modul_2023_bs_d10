# sisop_praktikum_modul_2023_BS_D10

## ANGGOTA KELOMPOK
1. Al-Ferro Yudisthira Putra
2. Bernisko Fancy Aljunez P.
3. Atha Dzaky

## Penjelasan Nomor 1
 1 A

`sort -t, -k21rn '2023 QS World University Rankings.csv' | awk -F',' '/JP/{print $2}' | head -5`

Sort pada line ini berguna untuk melakukan pengurutan, -t berguna untuk menunjukkan pemisah antar kolom pada file, -k21 berguna untuk melakukan sorting berdasarkan kolom ke 21, '2023 QS World University Rankings.csv' merupakan nama file yang digunakan, kemudian terdapat pipe terhadap fungsi awk guna untuk melakukan pencarian kata 'JP', apabila ditemukan maka akan di print kolom ke 2. Kemudian terdapat pipe kembali kepada head -5 yang bermakna untuk menunjukkan lima data teratas

 1 B

`sort -t, -k9n '2023 QS World University Rankings.csv' | awk -F',' '/JP/{print $2}' | head -5`

Sort pada line ini berguna untuk melakukan pengurutan, -t berguna untuk menunjukkan pemisah antar kolom pada file, -k9 berguna untuk melakukan sorting berdasarkan kolom ke 9, '2023 QS World University Rankings.csv' merupakan nama file yang digunakan, kemudian terdapat pipe terhadap fungsi awk guna untuk melakukan pencarian kata 'JP', apabila ditemukan maka akan di print kolom ke 2. Kemudian terdapat pipe kembali kepada head -5 yang bermakna untuk menunjukkan lima data teratas

 1 C

`sort -t, -k20,20n '2023 QS World University Rankings.csv' | awk -F',' '/Japan/ {print $2}' | head -10`

Sort pada line ini berguna untuk melakukan pengurutan, -t berguna untuk menunjukkan pemisah antar kolom pada file, -k20 berguna untuk melakukan sorting berdasarkan kolom ke 20, '2023 QS World University Rankings.csv' merupakan nama file yang digunakan, kemudian terdapat pipe terhadap fungsi awk guna untuk melakukan pencarian kata 'Japan', apabila ditemukan maka akan di print kolom ke 2. Kemudian terdapat pipe kembali kepada head -10 yang bermakna untuk menunjukkan lima data teratas

 1 D

`awk -F',' '/Keren/ {print $2}' '2023 QS World University Rankings.csv'`

awk pada line di atas berguna untuk menemukan kata 'Keren', -f berguna untuk menunjukan pembeda kolom yaitu ',', apabila ditemukan kata Keren maka akan di print kolom ke 2 dari file '2023 QS World University Rankings.csv'

## Penjelasan Nomor 2
### Poin a
Langkah pertama adalah memulai pengecekan apakah sebelumnya sudah ada folder kumpulan_nomorFolder di dalam directory. Jika sudah ada, harus dicari tau folder yang telah dibuat folder ke-berapa dan nomor folder untuk folder yang baru disimpan ke variabel no_folder. 
```bash
  no_folder=1

  while [ -d kumpulan_$no_folder ] 
  do no_folder=$((no_folder+1))
  done

```
Lalu, membuat folder atau directory baru dengan nomor folder yang telah didapatkan sebelumnya.
```bash
  mkdir kumpulan_$no_folder
```
Untuk mengetahui jumlah gambar yang akan diunduh ke dalam folder kumpulan_nomorFolder diperlukan waktu(jam) pada saat program dijalankan.
```bash
  WAKTU=$(date +"%H")
```
Setelah jumlah jam telah didapatkan, pindah directory ke folder kumpulan_nomorFolder. Lalu, melakukan pengecekan jika program dijalankan saat pukul 00.00, variabel waktu akan di-set menjadi 1. Lalu, beralih ke loop untuk mengunduh sebanyak jumlah jam dengan format nama perjalanan_nomorFile dan tipe file .jpg, dimana nomorFile berurut dari 1 sampai jumlah variabel waktu.

```bash
  cd kumpulan_$no_folder

  if [[ $WAKTU -eq 00:00 && $(date +"%H") ]]
  then
    WAKTU=1
  fi

  for ((no_file=1; no_file<=$WAKTU; no_file++))
  do
    wget -O perjalanan_$no_file.jpg https://loremflickr.com/320/240/indonesia
  done

```
### Crob Job
Agar program dijalankan setiap 10 jam sekali maka dalam waktu format cronjob adalah :
```bash
   * */10 * * * bash ~/kobeni.liburan.sh 1
```

### Poin B

Untuk membuat folder zip dari setiap folder kumpulan yang ada, diperlukan pengecekan folder kumpulan yang telah di-zip. Deklarasi variabel nomor untuk memeriksa secara urut dari folder kumpulan_1 hingga seterusnya apakah sudah tercipta file zip dengan format nama devil_nomor. Jika ditemukan folder kumpulan yang belum ada file zipnya maka program akan membuat file zipnya. Loop berjalan hingga tidak semua folder kumpulan sudah ada file zipnya.
```bash
   nomor=1
   while [ -d kumpulan_$nomor ]
   do 
     if [ -f devil_$nomor.zip ]
     then
       nomor=$((nomor+1))
       continue
     fi

     zip -r  devil_$nomor.zip kumpulan_$nomor
     nomor=$((nomor+1))
   done
```
### Cron Job
Agar program dijalankan setiap 1 hari sekali maka dalam waktu format cronjob adalah :
```bash
   @daily bash ~/kobeni_liburan.sh 2
```

## Penjelasan Nomor 3
### Penjelasan
Soal no 3 kita disuru untuk membuat sistem register (louis.sh) dan login (retep.sh).  

### Register
```bash
done=0

echo "Pendaftaran akun
====================="

until [ $done == 1 ]
do
    read -p "Username: " username;

    if grep -q "$username " ./users/user.txt
    then
        echo "$username sudah terdaftar silahkan gunakan yang lain"
        echo
        log_write "1"
    else 
        done=1
    fi
done
```
kode diatas terdapat difile `louis.sh`. kode tersebut digunakan untuk memasukan username yang ingin didaftarkan, jika username sudah digunakan maka program akan mengeluarkan `sudah terdaftar silahkan gunakan yang lain`.

tampilan ketika dijalankan

![alt text](https://cdn.discordapp.com/attachments/1083629376312180777/1083630790933487616/image.png)

jika username suda digunakan

![alt text](https://cdn.discordapp.com/attachments/1083629376312180777/1083630888568508476/image.png)

```bash
password="kosong"
repeat="null"

until [ "$password" == "$repeat" ]
do
    done=0

    until [ $done == 1 ]
    do
        read -s -p "Password: " password
        echo
        if [ ${#password} -lt 8 ]
        then
            pass_requirements
        elif [[ ! $(echo "$password" | awk '/[a-z]/ && /[A-Z]/ && /[0-9]/') ]]
        then
            pass_requirements
        elif [[ "$password" == "$username" ]]
        then
            printf "Password tidak boleh sama dengan username!\n\n"
        elif [[ "$password" == *$string1* ]]
        then
            printf "Password tidak boleh mengandung kata chicken!\n\n"
        elif [[ "$password" == *$string2* ]]
        then
            printf "Password tidak boleh mengandung kata ernie!\n\n"
        else
            done=1
        fi
    done

    read -s -p "Repeat your password:" repeat
    if [ $password != $repeat ]
        then printf "\nPassword tidak sama. Silahkan masukkan kembali password anda.\n\n"
    fi
    
done
```
Kode di atas untuk syarat membuat password. Password diperiksa apakah telah memenuhi syarat yang ditulis dalam soal *3 poin a*, yaitu:
Password harus terdiri dari:
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumeric
- Tidak boleh menggunakan kata chicken atau ernie

Contoh tampilan ketika password tidak memenuhi syarat:

![Sama dengan username](https://cdn.discordapp.com/attachments/1083629376312180777/1083632639673630732/image.png)

![Tidak sesuai syarat](https://cdn.discordapp.com/attachments/1083629376312180777/1083632779071340544/image.png)

![Mengandung kata erni/chicken](https://cdn.discordapp.com/attachments/1083629376312180777/1083633446821306428/image.png)

kode tersebut, user akan diminta untuk memasukkan passwordnya kembali. Jika tidak sama, user perlu memasukkan kembali passwordnya. Jika sudah sama, pendaftaran akan berhasil dan akun dapat digunakan.

Tampilan ketika password tidak sama:

![Repeat password tidak sama](https://cdn.discordapp.com/attachments/1083629376312180777/1083633851785543741/image.png)

Tampilan ketika pendaftaran berhasil:

![Pendaftaran berhasil](https://cdn.discordapp.com/attachments/1083629376312180777/1083634138889855006/image.png)

Tampilan di dalam user.txt:

![Users.txt](https://cdn.discordapp.com/attachments/1083629376312180777/1083634419295846440/image.png)

### retep
```bash
isLoggedIn=0

until [ $isLoggedIn == 1 ]
do
    read -p "Username: " username;
    read -p "Password: " password;


    if grep -q "$username " ./users/user.txt
    then
        if [ $password == $(awk -v id="$username " '$0 ~ id {print $2}' users/user.txt) ]
        then
            echo
            echo "Login berhasil!"
            isLoggedIn=1
            log_write "4" "$username" 
        else
            printf "\nPassword salah!\n\n"
            log_write "3" "$username" 
        fi
    else
        printf "\nUsername tidak ditemukan!\n\n"
    fi
done
```
kode di atas dipakai untuk melakukan login. User akan diminta memasukkan username dan password yang telah didaftarkan.

## Penjelasan Nomor 4

## Pada file encrypt didapati code sebagai berikut

!bash

```
lowerBet=({abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz})
upperBet=({ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ})
hour=$(date +"%H")
text=$(cat /var/log/syslog | tr "${lowerBet:0:26}${upperBet:0:26}" "${lowerBet:${hour}:26}${upperBet:${hour}:26}")

echo $text > "/media/psf/iCloud/Operating System/nomor4/encryption/$(date "+%H:%M %d:%m:%Y").txt"
```


lowerBet serta upperBet berguna untuk menyatakan huruf yang tersedia dari huruf kecil dan huruf besar, pernyataan huruf a-z dilakukan dua kali dikarenakan 
kemugnkinan untuk terjadi pengulangan setelah penambahan jam. Kemudian dinyatakan variabel hour dengan nilai waktu pada saat file di run. Setelah itu 
dinyatakan variabel text yang berisikan pembacaan file syslog yang kemudian di translate dari huruf awal menjadi huruf setelah penambahan jam. Kemudian 
variable text di echo kedalam format file penyimpanan yang berdasarkan waktu dan dalam format.txt.

## pada file decrypt diketahui code berupa

```
enFile=$(cat /media/psf/iCloud/Operating\ System/nomor4/encryption/*.txt | head -1)
hour=$(date +"%H")
lowerBet=({abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz})
upperBet=({ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ})

echo $enFile | tr "${lowerBet:${hour}:26}${upperBet:${hour}:26}" "${lowerBet:0:26}${upperBet:0:26}"
```


enFile berguna untuk mendapatkan file terbaru pada folder encryption, kemudian kita nyatakan variabel hour sebagai waku saat ini. nyatakan kembali lowerBet 
dan upperBet seperti di file Encrypt. Kemudian kita echo file yang didapati dari enFile yang kemudian di translate dari format waktu, menjadi format tanpa 
adanya waktu.

Crontab nya sebagai berikut

```
0 */2 * * * /bin.bash /media/psf/iCloud/Operating System/nomor4/log_encrypt.sh
0 */2 * * * /bin.bash /media/psf/iCloud/Operating System/nomor4/log_decrypt.sh
```


dimana kedua script akan di bash per 2 jam.

 





