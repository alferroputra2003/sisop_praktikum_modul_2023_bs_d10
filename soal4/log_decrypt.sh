#!bash

enFile=$(cat /media/psf/iCloud/Operating\ System/nomor4/encryption/*.txt | head -1)
hour=$(date +"%H")
lowerBet=({abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz})
upperBet=({ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ})

echo $enFile | tr "${lowerBet:${hour}:26}${upperBet:${hour}:26}" "${lowerBet:0:26}${upperBet:0:26}"

