#!/bin/bash

#define functions
pass_requirements() {
    echo "Password harus terdiri dari:
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumeric
- Tidak boleh menggunakan kata chicken atau ernie"
}
string1="chicken"
string2="ernie"

log_write() {
    currTime=`date +"%Y-%m-%d %T"`
    logAct=$1

    case "$logAct" in
    "1")
        echo "$currTime REGISTER: ERROR User already exists" >> log.txt
        ;;
    "2")
        echo "$currTime REGISTER: INFO User $2 registered successfully" >> log.txt
        ;;
    "3")
        echo "$currTime LOGIN: ERROR Failed login attempt on user $2 " >> log.txt
        ;;
    "4")
        echo "$currTime LOGIN: INFO User $2 logged in" >> log.txt
        ;;
    *)
        echo "_ERROR LOG_"
        ;;
    esac
}

done=0

echo "Pendaftaran akun
====================="

until [ $done == 1 ]
do
    read -p "Username: " username;

    if grep -q "$username " ./users/user.txt
    then
        echo "$username sudah terdaftar silahkan gunakan yang lain"
        echo
        log_write "1"
    else 
        done=1
    fi
done

password="kosong"
repeat="null"

until [ "$password" == "$repeat" ]
do
    done=0

    until [ $done == 1 ]
    do
        read -p "Password: " password
        echo
        if [ ${#password} -lt 8 ]
        then
            pass_requirements
        elif [[ ! $(echo "$password" | awk '/[a-z]/ && /[A-Z]/ && /[0-9]/') ]]
        then
            pass_requirements
        elif [[ "$password" == "$username" ]]
        then
            printf "Password tidak boleh sama dengan username!\n\n"
        elif [[ "$password" == *$string1* ]]
        then
            printf "Password tidak boleh mengandung kata chicken!\n\n"
        elif [[ "$password" == *$string2* ]]
        then
            printf "Password tidak boleh mengandung kata ernie!\n\n"
        else
            done=1
        fi
    done

    read -p "Repeat your password:" repeat
    if [ $password != $repeat ]
        then printf "\nPassword tidak sama. Silahkan masukkan kembali password anda.\n\n"
    fi
    
done

printf "\nSelamat pendaftaran akun anda telah berhasil!\n"
log_write "2" "$username"
echo "$username $password" >> users/user.txt

