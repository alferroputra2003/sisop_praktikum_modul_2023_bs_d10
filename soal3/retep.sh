#!/bin/bash

log_write() {
    currTime=`date +"%Y-%m-%d %T"`
    logAct=$1

    case "$logAct" in
    "1")
        echo "$currTime REGISTER: ERROR User already exists" >> log.txt
        ;;
    "2")
        echo "$currTime REGISTER: INFO User $2 registered successfully" >> log.txt
        ;;
    "3")
        echo "$currTime LOGIN: ERROR Failed login attempt on user $2 " >> log.txt
        ;;
    "4")
        echo "$currTime LOGIN: INFO User $2 logged in" >> log.txt
        ;;
    *)
        echo "_ERROR LOG_"
        ;;
    esac
}


isLoggedIn=0

until [ $isLoggedIn == 1 ]
do
    read -p "Username: " username;
    read -p "Password: " password;


    if grep -q "$username " ./users/user.txt
    then
        if [ $password == $(awk -v id="$username " '$0 ~ id {print $2}' users/user.txt) ]
        then
            echo
            echo "Login berhasil!"
            isLoggedIn=1
            log_write "4" "$username" 
        else
            printf "\nPassword salah!\n\n"
            log_write "3" "$username" 
        fi
    else
        printf "\nUsername tidak ditemukan!\n\n"
    fi
done
